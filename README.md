# Devoir 2

Ce dépôt contient l'énoncé et les fichiers nécessaires à la réalisation du
devoir 2 du cours INF5071 Infographie, de l'Université du Québec à Montréal,
enseigné à l'hiver 2018.

Le devoir doit être réalisé **seul**. Il doit être remis au plus tard le **6
mai 2018**, à 23h59. À partir de minuit, une pénalité de **2%** par heure de
retard sera appliquée.

## Remise du devoir

Afin de simplifier la remise, le devoir doit être remis par l'intermédiaire de
la plateforme GitLab. Pour ce faire, il suffit de cloner l'énoncé du devoir (ce
dépôt), de le rendre **privé**, puis de donner accès à l'utilisateur `ablondin`
(moi-même) en mode *Developer*. Les fichiers contenant les solutions aux
questions doivent se nommer `q1.py` et `q2.py`. Vous pouvez utilisez Python 2.7
ou Python 3.x, mais je vous demanderais d'indiquer laquelle des deux versions
vous avez utilisée lors de la remise.

Remplissez le gabarit [solution.md](solution.md) en complément aux fichiers
Python fournis pour répondre aux différentes questions.

## Question 1 (30 points)

Dans cette question, nous nous intéressons à la génération procédurale de
textures à l'aide de la bibliothèque
[Pillow](https://pillow.readthedocs.io/en/latest/).

### Sous-question 1.1 (15 points)

Produisez un script Python qui génère une carte de hauteurs (en anglais,
*height map*) en utilisant [l'algorithme
*diamond-square*](https://en.wikipedia.org/wiki/Diamond-square_algorithm).

**Note**: Comme cet algorithme est connu, il est probable qu'il existe des
solutions à ce problème sur le web. Je vous encourage cependant à ne pas en
tenir compte, car vous apprendrez beaucoup plus si vous tentez de résoudre le
problème vous-même. Dans tous les cas, n'oubliez pas de citer vos sources.

Nous nous attendons à ce que la commande
```
python q1.py heightmap 8 heightmap.png
```
produise une texture carrée de dimension $`2^8 + 1 \times 2^8 + 1`$ (le nombre
$`8`$ est la *profondeur* de l'image) avec niveau de gris et sauvegarde le
résultat dans le fichier `heightmap.png`. Voici quelques exemples de textures
produites avec ma solution (disponibles dans le dossier `exemples` de ce
dépôt):

| profondeur | image                        |
| :--------: | :--------------------------: |
| 2          | ![](exemples/heightmap2.png) |
| 3          | ![](exemples/heightmap3.png) |
| 4          | ![](exemples/heightmap4.png) |
| 5          | ![](exemples/heightmap5.png) |
| 6          | ![](exemples/heightmap6.png) |
| 7          | ![](exemples/heightmap7.png) |
| 8          | ![](exemples/heightmap8.png) |
| 9          | ![](exemples/heightmap9.png) |

Comme pour le premier devoir, vous n'êtes pas tenus de valider si l'utilisateur
entre des paramètres corrects lors de l'appel: vous pouvez prendre pour acquis
que le script sera testé avec des paramètres valides.

### Sous-question 1.2 (15 points)

Il est possible plusieurs façons de générer une carte de normales (qui est une
image en couleurs RGB) à partir d'une carte de hauteurs (qui est une image en
niveau de gris).

Dans cette sous-question, nous nous intéressons à l'une de ces façons, décrite
dans [une réponse sur
StackOverflow](https://stackoverflow.com/a/26357357/1060988).

Nous nous attendons en particulier à ce que la commande
```
python q1.py normal heightmap.png normal_map.png
```
produise une carte de normales et la sauvegarde dans le fichier
`normal_map.png` à partir de la carte de hauteurs données dans le fichier
`heightmap.png`.

Voici un exemple du résultat produit avec ma solution:

| profondeur | image                     |
| :--------: | :-----------------------: |
| 2          | ![](exemples/normal2.png) |
| 3          | ![](exemples/normal3.png) |
| 4          | ![](exemples/normal4.png) |
| 5          | ![](exemples/normal5.png) |
| 6          | ![](exemples/normal6.png) |
| 7          | ![](exemples/normal7.png) |
| 8          | ![](exemples/normal8.png) |
| 9          | ![](exemples/normal9.png) |

Notez que, dans les plus petites images, nous voyons clairement des "lignes"
obliques qui se distinguent, mais cela devient moins apparent dans les figures
de plus grande profondeur.
