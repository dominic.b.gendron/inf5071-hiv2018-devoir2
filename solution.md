# Solution au devoir 2

## Auteur

Indiquez ici votre prénom, votre nom et votre code permanent

## Solution à la question 1

Indiquez ici toute explication ou justification qui permet d'appuyer la réponse
que vous avez fournie pour la question 1. Référez au fichier qui contient votre
solution, par exemple [q1.py](q1.py). S'il y en a d'autres, n'hésitez pas à y
référer et à les expliquer brièvement. Attention aux fautes d'orthographe.
Notez qu'il n'est pas obligatoire de donner des explications très détaillées,
mais il faut minimalement indiquer la ou les commandes à entrer (et le résultat
attendu) pour vérifier que votre programme fonctionne bien.

## Solution à la question 2

Même chose pour la question 2

## Dépendances

Indiquez ici toutes les dépendances pour faire fonctionner votre projet. Une
dépendance est un logiciel ou une bibliothèque qui doit être installée pour
pouvoir reproduire les commandes et les résultats que vous obtenez.

## Références

Indiquez ici toute référence qui vous a permis de résoudre une ou plusieurs
questions.

## État du devoir

Indiquez ici si le devoir est complété, ou s'il y a certaines questions pour
lesquelles aucune réponse n'est fournie (ou simplement une réponse partielle).
